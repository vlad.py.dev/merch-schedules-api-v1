import sys

from django.utils.deprecation import MiddlewareMixin
from log.loggers import LogObject, ErrorLogObject
from .models import Log


class LoggingMiddleware(MiddlewareMixin):

    def process_request(self, request):
        # создать запись лога в БД
        log_instance = Log.objects.create()
        # присвоить id лога в БД атрибуту запроса
        request.__setattr__('request_guid', str(log_instance))

    def process_response(self, request, response):
        # обработать запрос, ответ
        log_obj = LogObject(request, response)
        log_data = log_obj.parse_request()
        log_data.update(log_obj.parse_response())
        log_instance = Log.objects.get(id=request.request_guid)

        if response.status_code == 500:
            return response

        log_obj.update_log_instance(log_instance, log_data)

        return response

    def process_exception(self, request, exception):
        exc_type, exc_value, exc_traceback = sys.exc_info()
        log_obj = ErrorLogObject(request, exc_type, exc_value, exc_traceback)
        log_data = log_obj.parse_request()
        log_data.update(log_obj.parse_exception())
        log_instance = Log.objects.get(id=request.request_guid)
        log_obj.update_log_instance(log_instance, log_data)
