import base64
import json
import traceback
from django.contrib.auth import get_user_model

from .models import Log
from json.decoder import JSONDecodeError


USER_MODEL = get_user_model()


class BaseLogObject:
    REQUEST_META_KEYS = ['REQUEST_METHOD', 'REMOTE_ADDR', 'PATH_INFO', 'HTTP_USER_AGENT']

    def __init__(self, request):
        self.request = request

    def get_user_from_jwt(self):
        request_headers = self.get_request_headers()
        token = request_headers.get('Authorization')
        if not token:
            return None
        token = token.split()[1]
        token_payload = json.loads(base64.urlsafe_b64decode(token.split('.')[1].encode()).decode())
        user_id = token_payload.get('user_id', None)
        user = USER_MODEL.objects.get(id=user_id)
        return user.username

    def get_user_from_request_body(self):
        try:
            request_body = dict(self.request._get_post())
        except JSONDecodeError:
            return None
        if request_body.get('username'):
            return request_body.get('username')[0]

    def get_user_from_request(self):
        return str(self.request.user)

    def get_user(self):
        user = self.get_user_from_jwt() or self.get_user_from_request_body() or self.get_user_from_request()
        return user

    def get_request_headers(self):
        return dict(self.request.headers)

    def get_request_params(self):
        return dict(self.request.GET)

    def parse_request(self):
        request_data = {key.lower(): value for key, value in self.request.META.items() if key in self.REQUEST_META_KEYS}
        request_data['user'] = self.get_user()
        request_data.update({
            'headers': json.dumps(self.get_request_headers(), ensure_ascii=False),
            'params': json.dumps(self.get_request_params(), ensure_ascii=False)
        })
        return request_data

    @staticmethod
    def get_log_instance(log_id):
        return Log.objects.get(id=log_id)

    @staticmethod
    def update_log_instance(log_instance, data):
        log_instance.request_method = data.get('request_method', None)
        log_instance.remote_addr = data.get('remote_addr', None)
        log_instance.path_info = data.get('path_info', None)
        log_instance.params = data.get('params', None)
        log_instance.headers = data.get('headers', None)
        log_instance.user = data.get('user', None)
        log_instance.status_code = data.get('status_code', None)
        log_instance.reason_phrase = data.get('reason_phrase', None)
        log_instance.user_errors = data.get('user_errors', None)
        log_instance.exc_type = data.get('exc_type', None)
        log_instance.exc_value = data.get('exc_value', None)
        log_instance.exc_traceback = data.get('exc_traceback', None)
        log_instance.save()


class LogObject(BaseLogObject):

    def __init__(self, request, response):
        BaseLogObject.__init__(self, request)
        self.response = response

    def parse_response(self):

        response_data = {
            'status_code': self.response.status_code,
            'reason_phrase': self.response.reason_phrase,
        }
        if self.response.status_code == 400:
            user_errors = self.response.data.get('errors') or self.response.data.get('detail')
            response_data.update({'user_errors': json.dumps(user_errors, ensure_ascii=False)})
        return response_data


class ErrorLogObject(BaseLogObject):
    def __init__(self, request, exc_type, exc_value, exc_traceback):
        BaseLogObject.__init__(self, request)
        self.exc_type = exc_type
        self.exc_value = exc_value
        self.exc_traceback = exc_traceback

    def parse_exception(self):
        tb = traceback.format_exception(self.exc_type, self.exc_value, self.exc_traceback)
        tb = [i.rstrip().lstrip() for i in tb]
        exc_traceback = '\n'.join(tb)
        exception_data = {
            'exc_type': str(self.exc_type),
            'exc_value': str(self.exc_value),
            'exc_traceback': exc_traceback
        }
        return exception_data
