from datetime import datetime

from rest_framework.exceptions import ParseError
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.parsers import JSONParser


# Create your views here.
# class TestView(APIView):
#
#     def get(self, request):
#         data = TestModel.objects.all()
#         serializer = TestModelSerializer(data, many=True)
#         return Response({
#             'request_guid': request.request_guid,
#             'timestamp': datetime.now(),
#             'data': serializer.data
#         }, status=status.HTTP_200_OK)
#
    # def post(self, request):
    #     try:
    #         serializer = TestModelSerializer(data=request.data, many=True)
    #     except ParseError as err:
    #         return Response({
    #             'request_guid': request.request_guid,
    #             'timestamp': datetime.now(),
    #             'status': 'error',
    #             'errors': str(err)
    #         }, status=status.HTTP_400_BAD_REQUEST)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response({
    #             'request_guid': request.request_guid,
    #             'timestamp': datetime.now(),
    #             'status': 'ok',
    #             'errors': None
    #         }, status=status.HTTP_201_CREATED)
    #     else:
    #         errors = [err for err in serializer.errors if err]
    #         return Response({
    #             'request_guid': request.request_guid,
    #             'timestamp': datetime.now(),
    #             'status': 'error',
    #             'errors': errors
    #         }, status=status.HTTP_400_BAD_REQUEST)
