"""
Задача должна принимать следующие аргументы:
    1. гуид запроса - ОБЯЗАТЕЛЬНО ПЕРВЫМ ПОЗИЦИОННЫМ АРГУМЕНТОМ
    2. тело запроса в формате json
    3. идентификатор пользователя
    4. параметры запроса
"""
import json
from datetime import date

from project.celery import app
from . import services, models


@app.task(serializer='json')
def open_access(request_guid, user_id, request_body, schedule_type, action):
    obj = services.OpenAccessScheduleProcessor(request_guid, user_id, request_body, schedule_type, action)
    result = obj.run_processing()
    return json.dumps(result, ensure_ascii=False)


@app.task(serializer='json')
def change_schedules(request_guid, user_id, request_body, schedule_type, action):
    obj = services.ChangeScheduleProcessor(request_guid, user_id, request_body, schedule_type, action)
    result = obj.run_processing()
    return json.dumps(result, ensure_ascii=False)


@app.task(serializer='json')
def dismissal_vacation_sick(request_guid, user_id, request_body, schedule_type, action):
    obj = services.DismissalVacationSickScheduleProcessor(request_guid, user_id, request_body, schedule_type, action)
    result = obj.run_processing()
    return json.dumps(result, ensure_ascii=False)


# @app.task(serializer='json')
# def check_vacation_sick():
#     today = date.today()
#     schedules = models.Schedule.objects.filter(is_vacation=True, is_sick=True, vacation_end=today)
#     if not schedules:
#         return 'not schedules'
#     schedules.update(is_active=True, is_vacation=False, is_sick=False, vacation_start=None, vacation_end=None)
#     return 'done'
