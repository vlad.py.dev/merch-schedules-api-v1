from django.urls import path

from . import views

urlpatterns = [
    # POST
    path('schedules/fixed/open-access/', views.OpenAccessFixedScheduleView.as_view()),
    path('schedules/sliding/open-access/', views.OpenAccessSlidingScheduleView.as_view()),
    path('schedules/fixed/change/', views.ChangeFixedScheduleView.as_view()),
    path('schedules/sliding/change/', views.ChangeSlidingScheduleView.as_view()),
    path('schedules/dismissal/', views.DismissalMerchandiserView.as_view()),
    path('schedules/vacation-sick/', views.VacationSickMerchandiserView.as_view()),
    # GET
    path('schedules/processing-result/', views.GetProcessingResultView.as_view()),
    path('merchandisers/all/', views.GetMerchandisersView.as_view()),
    path('merchandisers-schedules/', views.GetMerchandisersSchedulesView.as_view()),
    path('merchandisers-barcodes-schedules/', views.GetMerchandisersBarcodesSchedulesView.as_view()),
    path('merchandisers/about/<str:merch_barcode>/', views.GetMerchandiserFullInfoView.as_view(), name='get_full_merch_info'),
    # path('stores/', views.StoresView.as_view())
]
