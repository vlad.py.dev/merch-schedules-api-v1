import base64
import json
from datetime import datetime
from celery.result import AsyncResult
from django.core.exceptions import ObjectDoesNotExist
from django_celery_results.models import TaskResult
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from drf_yasg import openapi

from . import serializers
from . import services
from . import tasks
from project.swagger_config import (
    POST_REQUESTS, GET_REQUESTS, POST_RESPONSE,
    REQUEST_BODY_FIXED_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE,
    REQUEST_BODY_SLIDING_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE,
    REQUEST_BODY_DISMISSAL_SCHEDULE,
    REQUEST_BODY_VACATION_SICK, GET_RESPONSE_PROCESSING_RESULT,
    GET_MERCHANDISERS_ALL, GET_MERCHANDISERS_SCHEDULES, GET_MERCHANDISERS_SCHEDULES_BARCODES,
    GET_ONE_MERCHANDISER_INFO
)


# Create your views here.
class BaseMerchandiserSchedulesView(APIView):
    """
    Base class for all main api views.
    """

    def dispatch(self, request, *args, **kwargs):
        """Добавляем в объект запроса атрибут 'user_id'"""
        request.__setattr__('user_id', self.get_user_id_from_jwt(request))
        return super(BaseMerchandiserSchedulesView, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def make_response_post(request_guid, errors=None):
        """Метод формирует ответ для POST-запросов"""
        return {
            'request_guid': request_guid,
            'timestamp': datetime.now(),
            'status': 'processed',
            'errors': errors
        }

    @staticmethod
    def make_response_get(request, result):
        """Метод формирует ответ для GET-запросов"""
        return {
            'request_guid': request.request_guid,
            'timestamp': datetime.now(),
            'details': result
        }

    @staticmethod
    def get_task_id(request_guid):
        """
        Method for getting task ID by request number in logs.

        :param request_guid: the request_guid that is assigned in the logging middleware.
        :return:
        """
        template = '"[\'{}\']"'
        try:
            task = TaskResult.objects.get(task_args=template.format(request_guid))
        except ObjectDoesNotExist:
            return None
        return task.task_id

    @staticmethod
    def get_user_id_from_jwt(request):
        request_headers = request.headers
        token = request_headers.get('Authorization')
        if not token:
            return None
        token = token.split()[1]
        token_payload = json.loads(base64.urlsafe_b64decode(token.split('.')[1].encode()).decode())
        user_id = token_payload.get('user_id', None)
        return user_id


# ===== Классы для вставки/изменения данных ===== #
# 1. Класс для открытия доступа по фиксированному графику
class OpenAccessFixedScheduleView(BaseMerchandiserSchedulesView):
    SCHEDULE_TYPE = 'fixed'
    ACTION = 'open_access'

    @swagger_auto_schema(
        request_body=REQUEST_BODY_FIXED_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE,
        responses={200: POST_RESPONSE},
        tags=[POST_REQUESTS],
        operation_id='Оформление допуска по фиксированному графику'
    )
    def post(self, request):
        """Оформление допуска по фиксированному графику"""
        request_guid = request.request_guid
        result = tasks.open_access.apply_async(
            args=[request.request_guid],  # request_guid обязательно передавать первым позиционным аргументом!
            kwargs={
                'user_id': request.user_id,
                'request_body': request.data,
                'schedule_type': self.SCHEDULE_TYPE,
                'action': self.ACTION
            }
            # queue='api'
        )
        response = self.make_response_post(request_guid)
        return Response(response, status=status.HTTP_200_OK)


# 2. Класс для открытия доступа по плавающему графику
class OpenAccessSlidingScheduleView(BaseMerchandiserSchedulesView):
    SCHEDULE_TYPE = 'sliding'
    ACTION = 'open_access'

    @swagger_auto_schema(
        request_body=REQUEST_BODY_SLIDING_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE,
        responses={200: POST_RESPONSE},
        tags=[POST_REQUESTS],
        operation_id='Оформление допуска по плавающему графику'
    )
    def post(self, request):
        """Оформление допуска по плавающему графику"""
        request_guid = request.request_guid
        result = tasks.open_access.apply_async(
            args=[request.request_guid],  # request_guid обязательно передавать первым позиционным аргументом!
            kwargs={
                'user_id': request.user_id,
                'request_body': request.data,
                'schedule_type': self.SCHEDULE_TYPE,
                'action': self.ACTION
            },
            # queue='api'
        )
        response = self.make_response_post(request_guid)
        return Response(response, status=status.HTTP_200_OK)


class ChangeFixedScheduleView(BaseMerchandiserSchedulesView):
    SCHEDULE_TYPE = 'fixed'
    ACTION = 'change_schedule'

    @swagger_auto_schema(
        request_body=REQUEST_BODY_FIXED_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE,
        responses={200: POST_RESPONSE},
        tags=[POST_REQUESTS],
        operation_id='Изменение фиксированного графика'
    )
    def post(self, request):
        """Изменение фиксированного графика"""
        request_guid = request.request_guid
        result = tasks.change_schedules.apply_async(
            args=[request.request_guid],  # request_guid обязательно передавать первым позиционным аргументом!
            kwargs={
                'user_id': request.user_id,
                'request_body': request.data,
                'schedule_type': self.SCHEDULE_TYPE,
                'action': self.ACTION
            },
            queue='api'
        )
        response = self.make_response_post(request_guid)
        return Response(response, status=status.HTTP_200_OK)


class ChangeSlidingScheduleView(BaseMerchandiserSchedulesView):
    SCHEDULE_TYPE = 'sliding'
    ACTION = 'change_schedule'

    @swagger_auto_schema(
        request_body=REQUEST_BODY_SLIDING_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE,
        responses={200: POST_RESPONSE},
        tags=[POST_REQUESTS],
        operation_id='Изменение плавающего графика'
    )
    def post(self, request):
        """Изменение плавающего графика"""
        request_guid = request.request_guid
        result = tasks.change_schedules.apply_async(
            args=[request.request_guid],  # request_guid обязательно передавать первым позиционным аргументом!
            kwargs={
                'user_id': request.user_id,
                'request_body': request.data,
                'schedule_type': self.SCHEDULE_TYPE,
                'action': self.ACTION
            },
            queue='api'
        )
        response = self.make_response_post(request_guid)
        return Response(response, status=status.HTTP_200_OK)


class DismissalMerchandiserView(BaseMerchandiserSchedulesView):
    SCHEDULE_TYPE = 'none'
    ACTION = 'dismissal'

    @swagger_auto_schema(
        request_body=REQUEST_BODY_DISMISSAL_SCHEDULE,
        responses={200: POST_RESPONSE},
        tags=[POST_REQUESTS],
        operation_id='Увольнение мерчендайзера(-ов)'
    )
    def post(self, request):
        """Увольнение мерчендайзера(-ов)"""
        request_guid = request.request_guid
        result = tasks.dismissal_vacation_sick.apply_async(
            args=[request.request_guid],  # request_guid обязательно передавать первым позиционным аргументом!
            kwargs={
                'user_id': request.user_id,
                'request_body': request.data,
                'schedule_type': self.SCHEDULE_TYPE,
                'action': self.ACTION
            },
            queue='api'
        )
        response = self.make_response_post(request_guid)
        return Response(response, status=status.HTTP_200_OK)


class VacationSickMerchandiserView(BaseMerchandiserSchedulesView):
    SCHEDULE_TYPE = 'none'
    ACTION = 'dismissal'

    @swagger_auto_schema(
        request_body=REQUEST_BODY_VACATION_SICK,
        responses={200: POST_RESPONSE},
        tags=[POST_REQUESTS],
        operation_id='Оформление отпуска/больничного для мерчендайзера(-ов)'
    )
    def post(self, request):
        """Оформление отпуска/больничного для мерчендайзера(-ов)"""
        request_guid = request.request_guid
        result = tasks.dismissal_vacation_sick.apply_async(
            args=[request.request_guid],  # request_guid обязательно передавать первым позиционным аргументом!
            kwargs={
                'user_id': request.user_id,
                'request_body': request.data,
                'schedule_type': self.SCHEDULE_TYPE,
                'action': self.ACTION
            },
            queue='api'
        )
        response = self.make_response_post(request_guid)
        return Response(response, status=status.HTTP_200_OK)


# Классы для получения данных
# 1. получить результат обработки запроса
class GetProcessingResultView(BaseMerchandiserSchedulesView):
    """
    The class handles requests to get the result of processing an insert request.
    """
    @swagger_auto_schema(
        tags=[GET_REQUESTS],
        operation_id='Получить результат обработки запроса на вставку данных',
        responses={200: GET_RESPONSE_PROCESSING_RESULT}
    )
    def get(self, request):
        task_id = self.get_task_id(request.data.get('request_guid'))
        if not task_id:
            response = self.make_response_get(request, {'invalid_request_guid': request.data.get('request_guid')})
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        task_result = AsyncResult(task_id)
        if task_result.result.__class__.__name__ == 'str':
            response = self.make_response_get(request, json.loads(task_result.result))
        else:
            response = self.make_response_get(request, {'error': f"An error occurred while processing the request #'{request.data.get('request_guid')}'"})
        return Response(response, status=status.HTTP_200_OK)


# 2. получить всех мерчей (только данные мерча)
class GetMerchandisersView(BaseMerchandiserSchedulesView):

    @swagger_auto_schema(
        tags=[GET_REQUESTS],
        operation_id='Получить список всех мерчендайзеров',
        responses={200: GET_MERCHANDISERS_ALL}
    )
    def get(self, request):
        obj = services.GetMerchandisersSchedulesDataProcessor(request.user_id)
        data = obj.get_merchandisers()
        response = self.make_response_get(request, data)
        return Response(response, status=status.HTTP_200_OK)


# # 3. получить всех мерчей с графиками
class GetMerchandisersSchedulesView(BaseMerchandiserSchedulesView):

    @swagger_auto_schema(
        tags=[GET_REQUESTS],
        operation_id='Получить список всех мерчендайзеров с графиками',
        responses={200: GET_MERCHANDISERS_SCHEDULES}
    )
    def get(self, request):
        obj = services.GetMerchandisersSchedulesDataProcessor(request.user_id)
        data = obj.get_merchandisers_schedules()
        response = self.make_response_get(request, data)
        return Response(response, status=status.HTTP_200_OK)


# # 4. получить всех мерчей с графиками и ШК
class GetMerchandisersBarcodesSchedulesView(BaseMerchandiserSchedulesView):

    @swagger_auto_schema(
        tags=[GET_REQUESTS],
        operation_id='Получить список всех мерчендайзеров со штрихкодами и графиками',
        responses={200: GET_MERCHANDISERS_SCHEDULES_BARCODES}
    )
    def get(self, request):
        obj = services.GetMerchandisersSchedulesDataProcessor(request.user_id)
        data = obj.get_merchandisers_schedules_barcodes()
        response = self.make_response_get(request, data)
        return Response(response, status=status.HTTP_200_OK)


# # 5. получить данные одного мерча по его ID (с его ШК и графиками)
class GetMerchandiserFullInfoView(BaseMerchandiserSchedulesView):

    @swagger_auto_schema(
        tags=[GET_REQUESTS],
        operation_id='Получить информацию по одному мерчендайзеру',
        responses={200: GET_ONE_MERCHANDISER_INFO}
    )
    def get(self, request, merch_barcode):
        # params = request.query_params.dict()
        # ser = serializers.ScheduleTypeSerializer(data=params)
        # print(ser.is_valid())
        obj = services.GetMerchandisersSchedulesDataProcessor(user_id=request.user_id, barcode=merch_barcode)
        data = obj.get_full_merchandiser_info()
        response = self.make_response_get(request, data)
        return Response(response, status=status.HTTP_200_OK)
