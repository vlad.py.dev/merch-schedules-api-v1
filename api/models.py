import hashlib
import os
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models import Manager
from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse

from reportlab.graphics.barcode import code39
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas


USER_MODEL = get_user_model()


# Create your models here.
def hash_string(string: str):
    h = hashlib.sha1()
    h.update(string.encode('utf-8'))
    return h.hexdigest()


def create_barcode(barcode_str, merch_fio):
    absolute_barcode_dir = os.path.join(settings.MEDIA_ROOT, f"{barcode_str}.pdf")
    c = canvas.Canvas(os.path.join(absolute_barcode_dir), pagesize=A4)
    pdfmetrics.registerFont(TTFont('Play-Bold', settings.BARCODE_FONT_DIR))
    c.setFont('Play-Bold', 9)
    c.drawString(16 * mm, 280 * mm, merch_fio)
    c.drawString(16 * mm, 275 * mm, 'Мерчендайзер')
    # РАМКА
    c.line(10 * mm, 285 * mm, 90 * mm, 285 * mm)  # верхняя горизонтальная
    c.line(10 * mm, 245 * mm, 90 * mm, 245 * mm)  # нижняя горизонтальная
    c.line(10 * mm, 245 * mm, 10 * mm, 285 * mm)  # левая вертикальная
    c.line(90 * mm, 245 * mm, 90 * mm, 285 * mm)  # правая вертикальная
    # задать атрибуты документу
    c.setCreator('Magnit-API')
    c.setAuthor('Barcodes-App')
    c.setTitle(barcode_str)
    c.setProducer('Magnit-API')
    barcode = code39.Extended39(barcode_str, barWidth=.5 * mm, barHeight=15 * mm, humanReadable=True, checksum=False)
    # drawOn puts the barcode on the canvas at the specified coordinates
    barcode.drawOn(c, 10 * mm, 250 * mm)
    c.showPage()
    c.save()
    return absolute_barcode_dir


class MerchandiserEmployerBind(models.Model):
    merchandiser = models.ForeignKey('Merchandiser', on_delete=models.PROTECT)
    employer = models.ForeignKey(USER_MODEL, on_delete=models.PROTECT)
    barcode_str = models.CharField(max_length=35, null=True)
    barcode_dir = models.FilePathField(path=barcode_str, null=True)
    barcode_binary = models.BinaryField(null=True)

    objects = Manager()

    def __str__(self):
        return f'Merchandiser {self.merchandiser} of {self.employer}'

    @staticmethod
    def barcodes_dir():
        return os.path.join(settings.MEDIA_ROOT)

    def get_absolute_url(self):
        return reverse('get_full_merch_info', kwargs={'merch_barcode': self.barcode_str})

    @staticmethod
    def _processing_bind(bind):
        # 1. создать barcode_str
        bind.barcode_str = f'MERCH{bind.pk}'
        # 2. создать файл ШК
        barcode_dir = create_barcode(bind.barcode_str, str(bind.merchandiser))
        bind.barcode_dir = barcode_dir
        # записать бинарник в БД
        with open(barcode_dir, 'rb') as f:
            bind.barcode_binary = f.read()
        bind.save()

    def save(self, *args, **kwargs):
        super(MerchandiserEmployerBind, self).save(*args, **kwargs)
        # если еще нет ШК
        if not self.barcode_str:
            self._processing_bind(self)


class Merchandiser(models.Model):
    # сделать совместный индекс по фамилии, имени, отчеству, серии и номеру паспорта
    surname = models.CharField(max_length=155, verbose_name='surname')
    first_name = models.CharField(max_length=155, verbose_name='first_name')
    patronymic = models.CharField(max_length=155, verbose_name='patronymic', null=True, blank=True)
    passport_series = models.CharField(max_length=255, verbose_name='passport_series')
    passport_number = models.CharField(max_length=255, verbose_name='passport_number')
    phone = models.CharField(max_length=11, verbose_name='phone')
    employers = models.ManyToManyField(USER_MODEL, through='MerchandiserEmployerBind',
                                       through_fields=('merchandiser', 'employer'))
    black_list = models.BooleanField(default=False)
    request_id = models.CharField(max_length=55, verbose_name='request_id')

    objects = Manager()

    class Meta:
        unique_together = ['surname', 'first_name', 'patronymic', 'passport_series', 'passport_number']

    def __str__(self):
        if self.patronymic:
            return f'{self.surname} {self.first_name} {self.patronymic}'
        return f'{self.surname} {self.first_name}'

    def save(self, *args, **kwargs):
        self.passport_series = hash_string(self.passport_series)
        self.passport_number = hash_string(self.passport_number)
        super(Merchandiser, self).save(*args, **kwargs)


class Schedule(models.Model):
    WEEKDAYS = (
        ('monday', 'monday'),
        ('tuesday', 'tuesday'),
        ('wednesday', 'wednesday'),
        ('thursday', 'thursday'),
        ('friday', 'friday'),
        ('saturday', 'saturday'),
        ('sunday', 'sunday')
    )
    # общие поля
    store = models.ForeignKey('Store', on_delete=models.PROTECT, verbose_name='store')
    curator_email = models.EmailField(verbose_name='curator_email')
    curator_phone = models.CharField(max_length=11, verbose_name='curator_phone')
    work_start_date = models.DateField(verbose_name='work_start_date')
    visit_duration = models.PositiveSmallIntegerField(verbose_name='visit_duration')  # , validators=[MinValueValidator(1)]
    vacation_start = models.DateField(verbose_name='vacation_start', null=True, blank=True)
    vacation_end = models.DateField(verbose_name='vacation_end', null=True, blank=True)
    provider_name = models.CharField(max_length=155, verbose_name='provider_name')
    bind = models.ForeignKey(MerchandiserEmployerBind, on_delete=models.PROTECT, related_name='schedules', null=True, blank=True)
    # фикс. график
    visit_weekday = models.CharField(
        max_length=9, choices=WEEKDAYS, verbose_name='visit_weekday', null=True, blank=True
    )
    # плавающий график
    work_shifts = models.PositiveSmallIntegerField(
        verbose_name='work_shifts', null=True, blank=True, validators=[MinValueValidator(1), MaxValueValidator(7)]
    )
    weekend_shifts = models.PositiveSmallIntegerField(
        verbose_name='weekend_shifts', null=True, blank=True, validators=[MinValueValidator(1), MaxValueValidator(7)]
    )
    # служебные поля
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')
    is_active = models.BooleanField(default=True, verbose_name='is_active')
    is_vacation = models.BooleanField(default=False, verbose_name='is_vacation')
    is_sick = models.BooleanField(default=False, verbose_name='is_sick')
    request_id = models.CharField(max_length=55, verbose_name='request_id')

    objects = Manager()


class Store(models.Model):
    tt_name = models.CharField(max_length=155)
    tt_code = models.CharField(max_length=155)
    frmt = models.CharField(max_length=2)
    email = models.EmailField(null=True)

    objects = Manager()
# from api.models import *
# from users.models import CustomUser
# user = CustomUser.objects.get(id=1)
# merch = Merchandiser.objects.create(surname='Us', first_name='Vladislav', patronymic='Alexeevich', passport_series='0315', passport_number='211745', phone='89615861225', request_id='123456789')
# bind = MerchandiserEmployerBind.objects.create(merchandiser=merch, employer=user)
