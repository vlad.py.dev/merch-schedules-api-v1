from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.template.loader import render_to_string
from rest_framework.utils.serializer_helpers import ReturnDict, ReturnList
from datetime import date

from api import models
from . import serializers

USER_MODEL = get_user_model()


def format_serializer_errors(serializer_errors):
    """Метод форматирует ошибки сериализаторов в удобочитаемый вид"""
    if isinstance(serializer_errors, ReturnDict):
        return [f"{k}: {v[0]}" for k, v in serializer_errors.items()]
    if isinstance(serializer_errors, ReturnList):
        return [f"{k}: {v[0]}" for entry in serializer_errors for k, v in entry.items()]


class BaseMerchandiserScheduleProcessor:
    """
    Класс является базовым для классов-наследников, которые будут обрабатывать
    все запросы на запись/изменение данных в БД. Определяет ряд методов, которые будут
    общими для всех классов-наследников.
    """
    SCHEDULE_SERIALIZERS = {
        'fixed': serializers.InsertScheduleFixSerializer,
        'sliding': serializers.InsertScheduleSlidingSerializer,
        'dismissal': serializers.ScheduleDismissalSerializer,
        'vacation': serializers.ScheduleVacationSickSerializer,
        'sick_leave': serializers.ScheduleVacationSickSerializer
    }
    WORK_TIME_VALIDATION_INDICATORS = {
        'fixed': 'visit_weekday',
        'sliding': 'work_start_date'
    }
    WORK_TIME_VALIDATION_STANDARDS = {
        'fixed': 480,
        'sliding': 720
    }

    def __init__(self, request_guid, user_id, request_body, schedule_type, action=None):
        self.request_guid = request_guid
        self.action = action
        self.schedule_type = schedule_type
        self.request_body = request_body
        self.employer = self.get_user(user_id)
        self.schedule_serializer = self._get_schedule_serializer()
        self.errors = []

    @staticmethod
    def get_user(user_id):
        """Метод возвращает объект пользователя."""
        user = USER_MODEL.objects.get(id=user_id)
        return user

    @staticmethod
    def get_store(tt_name):
        return models.Store.objects.get(tt_name=tt_name)

    @staticmethod
    def send_mail_to_gm(mail_for_gm):
        message = None
        # 1. определить вид шаблона
        if mail_for_gm[0].get('visit_weekday'):
            message = render_to_string('email/fixed_schedule_open_access_email_body.txt', context={'data': mail_for_gm})
        if mail_for_gm[0].get('work_shifts'):
            message = render_to_string('email/sliding_schedule_open_access_email_body.txt', context={'data': mail_for_gm})
        if mail_for_gm[0].get('vacation_start'):
            # message = render_to_string('email/sliding_schedule_open_access_email_body.txt', context={'data': mail_for_gm})
            pass
        send_mail(
            subject='letter to GM',
            message=message,
            recipient_list=[mail_for_gm[0].get('store_email')],
            from_email='admin@mail.ru'
        )

    @staticmethod
    def get_merchandiser(surname, first_name, patronymic, passport_series, passport_number, **kwargs):
        try:
            merchandiser = models.Merchandiser.objects.get(
                surname=surname, first_name=first_name, patronymic=patronymic,
                passport_series=models.hash_string(passport_series), passport_number=models.hash_string(passport_number)
            )
        except ObjectDoesNotExist:
            return None
        return merchandiser

    def merchandiser_processing(self, merchandiser_serializer_validated_data):
        """
        Метод должен принимать .validated_data из сериализатора и принимать решение,
        как обрабатывать мерча. Логика для открытия доступа и изменения графика разная,
        поэтому метод сам по себе должен определяться в соответствующих классах-наследниках.
        """
        raise NotImplemented('The method must be defined in the inherited class')

    def _get_schedule_serializer(self):
        """Метод возвращает класс сериализатора согласно требуемому действию"""
        if self.action in ('open_access', 'change_schedule'):
            return self.SCHEDULE_SERIALIZERS.get(self.schedule_type)
        return self.SCHEDULE_SERIALIZERS.get(self.action)

    def run_mailing(self, created_schedules):
        data_for_mailing = [
            {
                'merchandiser': str(schedule.bind.merchandiser),
                'employer': schedule.provider_name,
                'store_email': schedule.store.email,
                'visit_duration': schedule.visit_duration,
                'visit_weekday': schedule.visit_weekday,
                'work_shifts': schedule.work_shifts,
                'weekend_shifts': schedule.weekend_shifts,
                'vacation_start': schedule.vacation_start,
                'vacation_end': schedule.vacation_end
            }
            for schedule in created_schedules if schedule.store.frmt == 'ГМ'
        ]
        unique_merchandisers = list(set([i.get('merchandiser') for i in data_for_mailing]))
        for merchandiser in unique_merchandisers:
            merchandiser_data = list(filter(lambda s: s.get('merchandiser') == merchandiser, data_for_mailing))
            unique_gm_emails = list(set([i.get('store_email') for i in merchandiser_data]))
            for email in unique_gm_emails:
                mail_for_gm = list(filter(lambda s: s.get('store_email') == email, data_for_mailing))
                self.send_mail_to_gm(mail_for_gm)
        return data_for_mailing

    def validate_work_time(self, bind, schedules):
        """
        Метод предназначен для проведения валидации рабочего времени,
        заявленного в присланном графике.
        """
        # получить поля графиков, которые хотят закачать
        fields = list(schedules[0].keys())
        # получить показатель, по которому будем проводить валидацию рабочего времени
        indicator = self.WORK_TIME_VALIDATION_INDICATORS.get(self.schedule_type)
        # получить стандарт рабочего времени
        standard = self.WORK_TIME_VALIDATION_STANDARDS.get(self.schedule_type)
        validated_data = []
        # 1 получить уникальные показатели для каждого вида графика
        unique_indicator_values = list(set([i.get(indicator) for i in schedules]))
        # далее в цикле по каждому уникальному показателю
        for value in unique_indicator_values:
            # отфильтровать данные по уникальному показателю
            grouped_schedules = list(filter(lambda s: s.get(indicator) == value, schedules))
            # получить из БД имеющиеся активные графики с таким же уникальным показателем
            existing_schedules = list(models.Schedule.objects.filter(bind=bind, **{indicator: value}).values(*fields))
            # объединить имеющиеся графики и графики на заливку
            existing_schedules.extend(grouped_schedules)
            # посчитать кол-во минут работы для каждого уникального показателя
            total_work_time = sum([i.get('visit_duration') for i in existing_schedules])
            if total_work_time <= standard:
                validated_data.extend(grouped_schedules)
                continue
            self.errors.append(
                f'The maximum operating time per day has been exceeded on {value}: {total_work_time} instead {standard}'
            )
        return validated_data


class OpenAccessScheduleProcessor(BaseMerchandiserScheduleProcessor):
    """Класс обрабатывает запросы на открытие доступа мерчендайзера на магазин"""

    def run_processing(self):
        """
        В методе реализована именно сама логика обработки данных
        для запросов на оформление допуска на ТТ.
        """
        created_merchandisers = []
        # для каждой записи из тела запроса
        for entry in self.request_body:
            # получить данные мерча
            merchandiser_section = entry.get('merchandiser', None)
            if not merchandiser_section:
                self.errors.append('No merchandiser data provided')
                continue
            # загнать мерча в сериализатор
            merchandiser_serializer = serializers.MerchandiserSerializer(data=merchandiser_section)
            # если данные в сериализаторе корректные
            merchandiser_serializer_valid = merchandiser_serializer.is_valid()
            if merchandiser_serializer_valid:
                # запустить обработчик данных мерча и получить связку мерч-поставщик
                merchandiser_employer_bind = self.merchandiser_processing(merchandiser_serializer.validated_data)
                # если вернулось None -> переходим к следующей секции данных
                if not merchandiser_employer_bind:
                    continue
                # добавить мерча из связки в список
                created_merchandisers.append(
                    {
                        'merchandiser': str(merchandiser_employer_bind.merchandiser),
                        'full_info': merchandiser_employer_bind.get_absolute_url()
                    }
                )
                # получить все графики из запроса
                schedules_to_upload = entry.get('schedules', None)
                if not schedules_to_upload:
                    self.errors.append('No schedules provided')
                    continue
                # создать переменную для сохранения очищенных через сериализатор данных
                validated_schedules_to_upload = []
                # прогнать графики из запроса через сериализатор по одному
                for schedule in schedules_to_upload:
                    schedule_serializer = self.schedule_serializer(data=schedule)
                    schedule_serializer_valid = schedule_serializer.is_valid()
                    if schedule_serializer_valid:
                        validated_schedules_to_upload.append(dict(schedule_serializer.validated_data))
                    if not schedule_serializer_valid:
                        self.errors.append(format_serializer_errors(schedule_serializer.errors))
                # проверить, имеются ли в БД графики по связке bind-tt_name-provider_name
                validated_schedules_to_upload = self.check_existing_schedules(
                    bind=merchandiser_employer_bind,
                    validated_schedules_to_upload=validated_schedules_to_upload
                )
                # проверить на корректность заявленное в запросе рабочее время
                if not validated_schedules_to_upload:
                    continue
                validated_schedules_to_upload = self.validate_work_time(merchandiser_employer_bind, validated_schedules_to_upload)
                # и вот только теперь можно залить графики в БД
                created_schedules = self.create_schedules(merchandiser_employer_bind, validated_schedules_to_upload)
                self.run_mailing(created_schedules)
            if not merchandiser_serializer_valid:
                self.errors.append(format_serializer_errors(merchandiser_serializer.errors))
        return {
            'status': 'done',
            'errors': self.errors,
            'barcodes': created_merchandisers
        }

    def merchandiser_processing(self, merchandiser_serializer_validated_data):
        """
        Этот метод вызывается уже после валидации сериализатора
        на вход принимает .validated_data из сериализатора
        отдает всегда объект связки мерч-поставщик
        есть три варианта:
            1. мерча вообще нет в БД
            2. мерч есть в БД и еще не привязан к данному поставщику
            3. мерч есть в БД и уже привязан к данному поставщику
        """
        merchandiser_serializer_validated_data = dict(merchandiser_serializer_validated_data)
        # 1. найти такого мерча в БД
        existing_merchandiser = self.get_merchandiser(**merchandiser_serializer_validated_data)
        # 2. если такого мерча в БД нет
        if not existing_merchandiser:
            # добавить в .validated_data request_guid
            merchandiser_serializer_validated_data.update({'request_id': self.request_guid})
            # создать мерча
            new_merchandiser = models.Merchandiser.objects.create(**merchandiser_serializer_validated_data)
            # создать связку мерч-поставщик
            merchandiser_employer_bind = models.MerchandiserEmployerBind.objects.create(merchandiser=new_merchandiser,
                                                                                        employer=self.employer)
            return merchandiser_employer_bind
        # 3. если такой мерч в БД есть:
        if existing_merchandiser:
            # если найденный мерч в ЧС
            if existing_merchandiser.black_list:
                self.errors.append(f'The merchandiser {merchandiser_serializer_validated_data.get("surname")} '
                                   f'{merchandiser_serializer_validated_data.get("first_name")} '
                                   f'{merchandiser_serializer_validated_data.get("patronymic")} '
                                   f'is in the problem base. Access to the outlet is closed')
                return None
            # если связка мерч-поставщик есть: найти ее и вернуть
            if self.employer in existing_merchandiser.employers.all():
                merchandiser_employer_bind = models.MerchandiserEmployerBind.objects.get(
                    merchandiser=existing_merchandiser,
                    employer=self.employer)
                return merchandiser_employer_bind
            # если связки мерч-поставщик нет - создать ее и вернуть
            if self.employer not in existing_merchandiser.employers.all():
                merchandiser_employer_bind = models.MerchandiserEmployerBind.objects.create(
                    merchandiser=existing_merchandiser,
                    employer=self.employer)
                return merchandiser_employer_bind

    def check_existing_schedules(self, bind: models.MerchandiserEmployerBind, validated_schedules_to_upload: list):
        """
        метод принимает к качестве аргументов: свзяка мерч-поставщик и очищенные через сериализатор графики
        проверяет, есть ли в БД графики по связке: связка мерч-поставщик - названия магазина - название поставщика
        """
        new_schedules = []
        for schedule in validated_schedules_to_upload:
            qs = self.get_matching_schedules(bind=bind, schedule=schedule)
            if not qs:
                new_schedules.append(schedule)
                continue
            if qs:
                self.errors.append(
                    f'Merchandiser {str(bind.merchandiser)} '
                    f'is already registered in the system from the specified supplier '
                    f'at the specified trading facility: {schedule.get("store").get("tt_name")}'
                )
        return new_schedules

    def create_schedules(self, bind, schedules):
        created_schedules = []
        for schedule in schedules:
            store = self.get_store(schedule.pop('store').get('tt_name'))
            schedule.update({'bind': bind, 'request_id': self.request_guid, 'store': store})
            created_schedules.append(models.Schedule.objects.create(**schedule))
        return created_schedules

    @staticmethod
    def get_matching_schedules(bind, schedule):
        tt_name = schedule.get('store').get('tt_name')
        provider_name = schedule.get('provider_name')
        return models.Schedule.objects.filter(
            bind=bind, store__tt_name=tt_name, provider_name=provider_name, is_active=True
        )


class ChangeScheduleProcessor(OpenAccessScheduleProcessor):
    """Класс обрабатывает запросы на изменение графика посещения работы мерча"""

    def run_processing(self):
        existing_merchandisers = []
        # для каждой записи из тела запроса
        for entry in self.request_body:
            # получить данные мерча
            merchandiser_section = entry.get('merchandiser', None)
            if not merchandiser_section:
                self.errors.append('No merchandiser data provided')
                continue
            # загнать мерча в сериализатор
            merchandiser_serializer = serializers.MerchandiserSerializer(data=merchandiser_section)
            # если данные в сериализаторе корректные
            merchandiser_serializer_valid = merchandiser_serializer.is_valid()
            if merchandiser_serializer_valid:
                # запустить обработчик данных мерча и проверить, есть ли в БД связка мерч-поставщик
                merchandiser_exists_bind = self.merchandiser_processing(merchandiser_serializer.validated_data)
                # если такой связки нет - перейти к следующей части графиков
                if not merchandiser_exists_bind:
                    continue
                # если такая связка есть - добавить в existing_merchandisers
                if isinstance(merchandiser_exists_bind, models.MerchandiserEmployerBind):
                    existing_merchandisers.append(
                        {
                            'merchandiser': str(merchandiser_exists_bind.merchandiser),
                            'full_info': merchandiser_exists_bind.get_absolute_url()
                        }
                    )
                # получить все графики из запроса
                schedules_to_upload = entry.get('schedules')
                if not schedules_to_upload:
                    self.errors.append('No schedules provided')
                # создать переменную для сохранения очищенных через сериализатор данных
                validated_schedules_to_upload = []
                # прогнать графики из запроса через сериализатор по одному
                for schedule in schedules_to_upload:
                    schedule_serializer = self.schedule_serializer(data=schedule)
                    schedule_serializer_valid = schedule_serializer.is_valid()
                    if schedule_serializer_valid:
                        validated_schedules_to_upload.append(dict(schedule_serializer.validated_data))
                    if not schedule_serializer_valid:
                        self.errors.append(format_serializer_errors(schedule_serializer.errors))
                # оставить только те графики, которые имеются в БД по связке bind-tt_name-provider_name
                validated_schedules_to_upload = self.check_existing_schedules(
                    bind=merchandiser_exists_bind,
                    validated_schedules_to_upload=validated_schedules_to_upload
                )
                if not validated_schedules_to_upload:
                    continue
                # проверить на корректность заявленное рабочее время
                if not validated_schedules_to_upload:
                    continue
                validated_schedules_to_upload = self.validate_work_time(merchandiser_exists_bind, validated_schedules_to_upload)
                # и вот только теперь можно залить графики в БД
                created_schedules = self.create_schedules(merchandiser_exists_bind, validated_schedules_to_upload)
                self.run_mailing(created_schedules=created_schedules)
            if not merchandiser_serializer_valid:
                self.errors.append(format_serializer_errors(merchandiser_serializer.errors))
        return {
            'status': 'done',
            'errors': self.errors,
            'barcodes': existing_merchandisers
        }

    def merchandiser_processing(self, merchandiser_serializer_validated_data):
        """
        Этот метод вызывается уже после валидации сериализатора
        на вход принимает .validated_data из сериализатора
        Если такая связка мерч-поставщик в БД есть - возвращает связку, если нет - None
        есть три варианта:
            1. мерча вообще нет в БД
            2. мерч есть в БД и еще не привязан к данному поставщику
            3. мерч есть в БД и уже привязан к данному поставщику
        """
        merchandiser_serializer_validated_data = dict(merchandiser_serializer_validated_data)
        # 1. найти такого мерча в БД
        existing_merchandiser = self.get_merchandiser(**merchandiser_serializer_validated_data)
        # 2. если такого мерча в БД нет
        if not existing_merchandiser:
            self.errors.append(
                f'The merchandiser {merchandiser_serializer_validated_data.get("surname")} '
                f'{merchandiser_serializer_validated_data.get("first_name")} '
                f'{merchandiser_serializer_validated_data.get("patronymic")} '
                f'is not registered in the system'
            )
            return None
        # 3. если такой мерч в БД есть:
        if existing_merchandiser:
            # если данный мерч в ЧС
            if existing_merchandiser.black_list:
                self.errors.append(
                    f'The merchandiser {merchandiser_serializer_validated_data.get("surname")} '
                    f'{merchandiser_serializer_validated_data.get("first_name")} '
                    f'{merchandiser_serializer_validated_data.get("patronymic")} '
                    f'is in the problem base. Access to the outlet is closed'
                )
                return None
            # если связка мерч-поставщик есть: найти ее и вернуть
            if self.employer in existing_merchandiser.employers.all():
                merchandiser_employer_bind = models.MerchandiserEmployerBind.objects.get(
                    merchandiser=existing_merchandiser,
                    employer=self.employer)
                return merchandiser_employer_bind
            # если связки мерч-поставщик нет
            if self.employer not in existing_merchandiser.employers.all():
                self.errors.append(f"The merchandiser {merchandiser_serializer_validated_data.get('surname')} "
                                   f"{merchandiser_serializer_validated_data.get('first_name')} "
                                   f"{merchandiser_serializer_validated_data.get('patronymic')} "
                                   f"is not registered in the system from the supplier {str(self.employer)}")
                return None

    def check_existing_schedules(self, bind: models.MerchandiserEmployerBind, validated_schedules_to_upload: list):
        """
        метод принимает к качестве аргументов: свзяка мерч-поставщик и очищенные через сериализатор графики
        проверяет, есть ли в БД графики по связке: связка мерч-поставщик - названия магазина - название поставщика
        """
        matching_schedules = []  # тут будут графики из запроса
        for schedule in validated_schedules_to_upload:
            qs = self.get_matching_schedules(bind, schedule)
            if not qs:
                self.errors.append(
                    f'Merchandiser {str(bind.merchandiser)} '
                    f'is not registered in the system from the specified supplier: {str(self.employer)} '
                    f'at the specified trading facility: {schedule.get("tt_name")}'
                )
                continue
            if qs:
                matching_schedules.append(schedule)
                continue
        self.disable_schedules(bind, matching_schedules)  # метим графики неактивными
        return matching_schedules

    def disable_schedules(self, bind, matching_schedules):
        for schedule in matching_schedules:
            qs = self.get_matching_schedules(bind, schedule)
            qs.update(is_active=False)


class DismissalVacationSickScheduleProcessor(ChangeScheduleProcessor):
    """Класс для обработки запросов на увольнение мерча(-ей)"""

    def run_processing(self):
        existing_merchandisers = []
        # для каждой записи из тела запроса
        for entry in self.request_body:
            # получить данные мерча
            merchandiser_section = entry.get('merchandiser')
            if not merchandiser_section:
                self.errors.append('No merchandiser data provided')
                continue
            # загнать мерча в сериализатор
            merchandiser_serializer = serializers.MerchandiserSerializer(data=merchandiser_section)
            # если данные в сериализаторе корректные
            merchandiser_serializer_valid = merchandiser_serializer.is_valid()
            if merchandiser_serializer_valid:
                # запустить обработчик данных мерча и проверить, есть ли в БД связка мерч-поставщик
                merchandiser_exists_bind = self.merchandiser_processing(merchandiser_serializer.validated_data)
                # если такой связки нет - добавить ошибку в self.errors и перейти к следующей части графиков
                if not merchandiser_exists_bind:
                    continue
                # если такая связка есть - добавить в existing_merchandisers
                if isinstance(merchandiser_exists_bind, models.MerchandiserEmployerBind):
                    existing_merchandisers.append(
                        {
                            'merchandiser': str(merchandiser_exists_bind.merchandiser),
                            'full_info': merchandiser_exists_bind.get_absolute_url()
                        }
                    )
                # получить все графики из запроса
                schedules_to_upload = entry.get('schedules')
                if not schedules_to_upload:
                    self.errors.append('No schedules provided')
                    continue
                # создать переменную для сохранения очищенных через сериализатор данных
                validated_schedules_to_upload = []
                # прогнать графики из запроса через сериализатор по одному
                for schedule in schedules_to_upload:
                    schedule_serializer = self.schedule_serializer(data=schedule)
                    schedule_serializer_valid = schedule_serializer.is_valid()
                    if schedule_serializer_valid:
                        validated_schedules_to_upload.append(dict(schedule_serializer.validated_data))
                    if not schedule_serializer_valid:
                        self.errors.append(format_serializer_errors(schedule_serializer.errors))
                # оставить только те графики, которые имеются ли в БД по связке bind-tt_name-provider_name
                if not validated_schedules_to_upload:
                    continue
                validated_schedules_to_upload = self.check_existing_schedules(
                    bind=merchandiser_exists_bind,
                    validated_schedules_to_upload=validated_schedules_to_upload
                )
            if not merchandiser_serializer_valid:
                self.errors.append(format_serializer_errors(merchandiser_serializer.errors))
        return {
            'status': 'done',
            'errors': self.errors,
            'barcodes': existing_merchandisers
        }

    def check_existing_schedules(self, bind: models.MerchandiserEmployerBind, validated_schedules_to_upload: list):
        """
        метод принимает к качестве аргументов: свзяка мерч-поставщик и очищенные через сериализатор графики
        проверяет, есть ли в БД графики по связке: связка мерч-поставщик - названия магазина - название поставщика
        """
        matching_schedules = []  # тут будут графики из запроса
        for schedule in validated_schedules_to_upload:
            qs = self.get_matching_schedules(bind, schedule)
            if not qs:
                self.errors.append(
                    f'Merchandiser {str(bind.merchandiser)} '
                    f'is not registered in the system from the specified supplier: {str(self.employer)} '
                    f'at the specified trading facility: {schedule.get("tt_name")}'
                )
                continue
            if qs:
                matching_schedules.append(schedule)
                continue
        self.disable_schedules(bind, matching_schedules)
        if self.action == 'vacation':
            self.make_vacation(bind, matching_schedules)
        if self.action == 'sick_leave':
            self.make_sick_leave(bind, matching_schedules)
        return matching_schedules

    def make_vacation(self, bind, matching_schedules):
        for schedule in matching_schedules:
            queryset = self.get_matching_schedules(bind, schedule)
            queryset.update(
                is_active=False, is_vacation=True,
                vacation_start=schedule.get('vacation_start'), vacation_end=schedule.get('vacation_end')
            )

    def make_sick_leave(self, bind, matching_schedules):
        for schedule in matching_schedules:
            queryset = self.get_matching_schedules(bind, schedule)
            queryset.update(
                is_active=False, is_sick=True,
                vacation_start=schedule.get('vacation_start'), vacation_end=schedule.get('vacation_end')
            )


class GetMerchandisersSchedulesDataProcessor:

    def __init__(self, user_id, barcode=None):
        self.user_id = user_id
        self.barcode = barcode

    @staticmethod
    def get_binds(user_id):
        return models.MerchandiserEmployerBind.objects.filter(employer_id=user_id)

    def get_merchandisers(self):
        binds = self.get_binds(user_id=self.user_id)
        merchandisers = [bind.merchandiser for bind in binds]
        serializer = serializers.MerchandiserSerializer(merchandisers, many=True)
        return serializer.data

    def get_merchandisers_schedules(self):
        binds = self.get_binds(user_id=self.user_id)
        total_data = []
        for bind in binds:
            merchandiser = bind.merchandiser
            schedules = bind.schedules.all()
            merchandiser_serializer = serializers.MerchandiserSerializer(merchandiser)
            schedules_serializer = serializers.GetSchedulesSerializer(schedules, many=True)
            total_data.append({
                'merchandiser': merchandiser_serializer.data,
                'schedules': schedules_serializer.data
            })
        return total_data

    def get_merchandisers_schedules_barcodes(self):
        binds = self.get_binds(user_id=self.user_id)
        total_data = []
        for bind in binds:
            merchandiser = bind.merchandiser
            schedules = bind.schedules.all()
            merchandiser_serializer = serializers.MerchandiserSerializer(merchandiser)
            schedules_serializer = serializers.GetSchedulesSerializer(schedules, many=True)
            barcode_serializer = serializers.BarcodeSerializer(bind)
            total_data.append({
                'merchandiser': merchandiser_serializer.data,
                'barcode_binary': barcode_serializer.data.get('barcode_binary'),
                'schedules': schedules_serializer.data,
            })
        return total_data

    def get_full_merchandiser_info(self):
        binds = self.get_binds(self.user_id)
        try:
            current_bind = binds.get(barcode_str__icontains=self.barcode)
        except ObjectDoesNotExist:
            return {'error': f"Merchandiser with barcode '{self.barcode}' is not registered from the specified vendor"}
        serializer = serializers.FullMerchandiserInfoSerializer(current_bind)
        return serializer.data


# from mimesis import Person, locales
# import json, requests
# from api.models import Store
#
# person = Person(locales.RU)
#
# stores = list(Store.objects.all())
#
# merch_data = {
#     'surname': person.last_name(),
#     'first_name': person.name(),
#     'patronymic': person.last_name(),
#     'passport_series': '1234',
#     'passport_number': '123456',
#     'phone': '89615861225'
# }
# schedules = []
# for s in stores:
#     schedule = {
#         'tt_name': s.tt_name,
#         'provider_name': 'Bolsius Polska Sp z o o',
#         'curator_email': 'curator@mail.ru',
#         'curator_phone': '89995557766',
#         'work_start_date': '2020-02-25',
#         'visit_duration': 0,
#         'visit_weekday': 'monday'
#     }
#     schedules.append(schedule)
#
# merchandisers = []
#
# for i in range(10):
#     merchandisers.append(
#         {
#             'merchandiser': {
#                 'surname': person.last_name(),
#                 'first_name': person.name(),
#                 'patronymic': person.last_name(),
#                 'passport_series': '1234',
#                 'passport_number': '123456',
#                 'phone': '89615861225'
#             },
#             'schedules': schedules
#         }
#
#     )
#
# with open('testing.json', 'w', encoding='utf-8') as f:
#     f.write(json.dumps(merchandisers, ensure_ascii=False, indent=4))
#
# with open('testing.json', encoding='utf-8') as f:
#     data = f.read().encode()
#
# url = 'http://127.0.0.1:8000/api/v1/schedules/fixed/open-access/'
# headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE1OTAwNjU3LCJqdGkiOiI4YjcyNTM5OGQ0YzM0ZTYxOGY5ZTFlZGQyNjgwNjM3MiIsInVzZXJfaWQiOjJ9.B9igSTU50DndVZnEOL3ggkKS7tFxQgiqUnI3vN3ar2s'}
# response=requests.request('POST', url=url, headers=headers, data=data)
#
#

