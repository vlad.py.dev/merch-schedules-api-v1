from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers

from api.models import Merchandiser, Schedule, MerchandiserEmployerBind, Store
from users.models import Provider


class MerchandiserSerializer(serializers.ModelSerializer):
    """Сериализация объекта мерчендайзера"""
    passport_series = serializers.CharField(max_length=155, write_only=True)
    passport_number = serializers.CharField(max_length=155, write_only=True)

    class Meta:
        model = Merchandiser
        fields = ['surname', 'first_name', 'patronymic', 'passport_series', 'passport_number', 'phone']


class GetSchedulesSerializer(serializers.ModelSerializer):
    """Сериализация объекта графика при запросах на получение графиков"""
    tt_name = serializers.CharField(max_length=155, source='store.tt_name')

    class Meta:
        model = Schedule
        exclude = ['id', 'created_at', 'updated_at', 'request_id', 'bind', 'store']


class BarcodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = MerchandiserEmployerBind
        fields = ['barcode_binary']


class FullMerchandiserInfoSerializer(serializers.ModelSerializer):
    """Сериализация связки мерч-поставщик: мерч, его ШК и все его графики"""
    merchandiser = MerchandiserSerializer()
    schedules = GetSchedulesSerializer(many=True, read_only=True)

    class Meta:
        model = MerchandiserEmployerBind
        fields = ['merchandiser', 'barcode_binary', 'schedules']


class BaseInsertScheduleSerializer(serializers.ModelSerializer):
    """Базовый класс сериализатора для графиков при вставке"""

    @staticmethod
    def check_store(store):
        try:
            Store.objects.get(tt_name=store)
        except ObjectDoesNotExist:
            return False
        return True

    @staticmethod
    def check_provider_name(provider_name):
        try:
            Provider.objects.get(provider_name=provider_name)
        except ObjectDoesNotExist:
            return False
        return True

    def validate_tt_name(self, value):
        if not self.check_store(value):
            raise serializers.ValidationError(f'Invalid store name: {value}')
        return value

    def validate_provider_name(self, value):
        if not self.check_provider_name(value):
            raise serializers.ValidationError(f'Invalid provider name: {value}')
        return value


class StoreSerializer(serializers.ModelSerializer):

    class Meta:
        model = Store
        fields = ['tt_name']


class InsertScheduleFixSerializer(BaseInsertScheduleSerializer):
    """Сериализатор для фиксированных графиков"""
    tt_name = serializers.CharField(max_length=155, source='store.tt_name')

    class Meta:
        model = Schedule
        fields = [
            'tt_name', 'provider_name', 'curator_email', 'curator_phone',
            'work_start_date', 'visit_duration', 'visit_weekday',
        ]
        extra_kwargs = {
            'visit_weekday': {'required': True}
        }


class InsertScheduleSlidingSerializer(BaseInsertScheduleSerializer):
    """Сериализатор для плавающих графиков"""
    tt_name = serializers.CharField(max_length=155, source='store.tt_name')

    class Meta:
        model = Schedule
        fields = [
            'tt_name', 'provider_name', 'curator_email', 'curator_phone',
            'work_start_date', 'visit_duration', 'work_shifts', 'weekend_shifts'
        ]
        extra_kwargs = {
            'work_shifts': {'required': True},
            'weekend_shifts': {'required': True}
        }


class ScheduleDismissalSerializer(serializers.ModelSerializer):
    """Сериализатор для графиков на увольнение"""

    class Meta:
        model = Schedule
        fields = ['tt_name', 'provider_name']


class ScheduleVacationSickSerializer(serializers.ModelSerializer):
    """Сериализатор для графиков на отпуск/больничный"""

    class Meta:
        model = Schedule
        fields = ['tt_name', 'provider_name', 'vacation_start', 'vacation_end']
