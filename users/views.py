from datetime import datetime

from rest_framework import status
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import TokenError, InvalidToken
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from drf_yasg.utils import swagger_auto_schema

from project.swagger_config import AUTH_REQUESTS


# Create your views here.
class CustomTokenObtainPairView(TokenObtainPairView):
    @swagger_auto_schema(
        security=[],
        tags=[AUTH_REQUESTS],
        operation_id='Получить access/refresh токены'
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])

        return Response(
            data={
                'request_guid': request.request_guid,
                'timestamp': datetime.now(),
                'token_pair': serializer.validated_data
            },
            status=status.HTTP_200_OK
        )


class CustomTokenRefreshView(TokenRefreshView):

    @swagger_auto_schema(
        security=[],
        tags=[AUTH_REQUESTS],
        operation_id='Освежить access/refresh токены'
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])

        return Response(
            data={
                'request_guid': request.request_guid,
                'timestamp': datetime.now(),
                'token_pair': serializer.validated_data
            },
            status=status.HTTP_200_OK
        )