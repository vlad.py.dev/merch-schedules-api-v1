from uuid import uuid4
from django.core.mail import send_mail
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db.models import Manager
from django.utils import timezone

from .managers import CustomUserManager


# Create your models here.
class CustomUser(AbstractBaseUser, PermissionsMixin):
    ROLES = (
        ('agency', 'Agency'),
        ('provider', 'Provider'),
        ('admin', 'Admin'),
    )
    username = models.CharField(max_length=155, unique=True, verbose_name='Username')
    email = models.EmailField(max_length=255, unique=True, verbose_name='Email')
    role = models.CharField(max_length=10, choices=ROLES, verbose_name='Role')
    full_name = models.CharField(max_length=155)
    vendor_name = models.CharField(max_length=155, null=True, blank=True, verbose_name='Vendor')

    date_joined = models.DateField(default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email', 'role', 'full_name', 'vendor_name']

    objects = CustomUserManager()

    class Meta:
        unique_together = ('full_name', 'vendor_name')

    def __str__(self):
        return self.username

    def __repr__(self):
        return self.username

    def email_user(self, subject, message, from_email='admin@mail.ru'):
        send_mail(subject=subject, message=message, from_email=from_email, recipient_list=[self.email])

    @staticmethod
    def make_random_password():
        return str(uuid4())


class Provider(models.Model):
    provider_name = models.CharField(max_length=155, unique=True)
    vendor_name = models.CharField(max_length=155)

    objects = Manager()

    def __str__(self):
        return self.provider_name
