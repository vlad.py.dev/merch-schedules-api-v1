from django.contrib.auth.models import BaseUserManager

from . import tasks


class CustomUserManager(BaseUserManager):

    def create_user(self, username, email, password=None, **kwargs):
        if not username and email:
            raise ValueError('Please, type username and (or) email')

        user = self.model(username=username, email=email, **kwargs)
        if not password:
            password = user.make_random_password()
        user.set_password(password)
        user.save()
        tasks.send_password.delay(username=username, email=email, password=password, action='user_added')
        return user

    def create_superuser(self, username, email, password=None, **kwargs):
        user = self.create_user(username=username, email=email, password=password, **kwargs)
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user
