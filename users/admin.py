from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from uuid import uuid4

from .forms import UserChangeForm, UserCreationForm
from .models import CustomUser
from . import tasks


# Register your models here.
def reset_password(modeladmin, request, queryset):
    for user in queryset:
        new_password = str(uuid4())
        user.set_password(new_password)
        user.save()
        tasks.send_password.apply_async(
            kwargs={
                'username': user.username,
                'email': user.email,
                'password': new_password,
                'action': 'password_reset'
            },
            queue='users'
        )


class UserAdmin(BaseUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    actions = [reset_password]
    readonly_fields = ['last_login', 'date_joined']
    list_display = ('email', 'username', 'full_name', 'vendor_name', 'role', 'is_staff', 'is_superuser', 'is_active')
    list_filter = ('role', 'is_staff', 'is_superuser')
    fieldsets = (
        ('Personal info', {'fields': ('username', 'email', 'full_name', 'vendor_name', 'role', 'last_login', 'date_joined')}),
        ('Permissions', {'fields': ('is_staff', 'is_superuser')}),
        ('Groups', {'fields': ('groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'full_name', 'vendor_name', 'role', 'user_permissions'),
        }),
    )
    search_fields = ('username', 'email', 'role', 'vendor_name', 'full_name')
    ordering = ('email', )
    filter_horizontal = ('groups', 'user_permissions',)


admin.site.register(CustomUser, UserAdmin)
