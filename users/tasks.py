from django.template.loader import render_to_string
from django.core.mail import send_mail

from project.celery import app


TEMPLATES_MAPPING = {
    'user_added': {
        'subject': 'email/password_email_subject.txt',
        'body': 'email/password_email_body.txt'
    },
    'password_reset': {
        'subject': 'email/password_reset_subject.txt',
        'body': 'email/password_reset_body.txt'
    }
}


@app.task
def send_password(username, email, password, action):
    context = {'username': username, 'password': password}
    subject = render_to_string(TEMPLATES_MAPPING[action]['subject'], context)
    body = render_to_string(TEMPLATES_MAPPING[action]['body'], context)
    return send_mail(
        subject=subject, message=body, from_email='admin@mail.ru', recipient_list=[email]
    )
