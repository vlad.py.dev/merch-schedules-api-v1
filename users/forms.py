from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from . import models
from . import tasks


class UserCreationForm(forms.ModelForm):

    class Meta:
        model = models.CustomUser
        fields = ('email', 'full_name', 'role', 'vendor_name')

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        password = user.make_random_password()
        user.set_password(password)
        if self.cleaned_data.get('role') == 'admin':
            user.is_staff = True
        if commit:
            user.save()
        tasks.send_password.apply_async(
            kwargs={
                'username': user.username,
                'password': password,
                'email': user.email,
                'action': 'user_added'
            },
            queue='users'
        )
        if user.role == 'provider':
            models.Provider.objects.create(provider_name=user.full_name, vendor_name=user.vendor_name)
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = models.CustomUser
        fields = ('email', 'full_name', 'vendor_name', 'role', 'is_active', 'is_staff', 'is_superuser')
