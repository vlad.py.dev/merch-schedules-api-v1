import os
from celery import Celery
from datetime import timedelta

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')

app = Celery('project')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.task_routes = {
    'api.tasks.*': {'queue': 'api'},
    'users.tasks.*': {'queue': 'users'}
}

# app.conf.beat_schedule = {
#     'check_vacation_sick_every_day': {
#         'task': 'api.tasks.check_vacation_sick',
#         'schedule': timedelta(days=1)
#     }
# }

# celery -A project worker -l info --pool=solo
# celery -A project beat -l debug
