from django.urls import path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

API_TITLE = 'Magnit-API'
API_DESCRIPTION = 'A Web api for information exchange with suppliers'


schema_view = get_schema_view(
    openapi.Info(
        title=API_TITLE,
        default_version='v1',
        description=API_DESCRIPTION,
        contact=openapi.Contact(email='us_va@magnit.ru'),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

swagger_urls = [
    path('docs/swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='swagger-ui'),
    path('docs/redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='redoc'),
]

AUTH_REQUESTS = 'Авторизация'
POST_REQUESTS = 'Запросы на вставку данных'
GET_REQUESTS = 'Запросы на получение данных'

# ОПИСАНИЕ БАЗОВЫХ СУЩНОСЕЙ, КОТОРЫЕ БУДУТ ИСПОЛЬЗОВАТЬСЯ ПРИ ДОКУМЕНТИРОВАНИИ КАЖДОГО ЗАПРОСА

# сущность мерчендайзера. основана на api.models.Merchandiser
MERCHANDISER_POST = openapi.Schema(
    title='Данные мерчендайзера',
    type=openapi.TYPE_OBJECT,
    properties={
        'surname': openapi.Schema(type=openapi.TYPE_STRING, description='Фамилия'),
        'first_name': openapi.Schema(type=openapi.TYPE_STRING, description='Имя'),
        'patronymic': openapi.Schema(type=openapi.TYPE_STRING, description='Отчество'),
        'passport_series': openapi.Schema(type=openapi.TYPE_STRING, description='Серия паспорта'),
        'passport_number': openapi.Schema(type=openapi.TYPE_STRING, description='Номер паспорта'),
        'phone': openapi.Schema(type=openapi.TYPE_STRING, description='Моб. телефон'),
    },
    required=['surname', 'first_name', 'passport_series', 'passport_number', 'phone']
)

MERCHANDISER_GET = openapi.Schema(
    title='Данные мерчендайзера',
    type=openapi.TYPE_OBJECT,
    properties={
        'surname': openapi.Schema(type=openapi.TYPE_STRING, description='Фамилия'),
        'first_name': openapi.Schema(type=openapi.TYPE_STRING, description='Имя'),
        'patronymic': openapi.Schema(type=openapi.TYPE_STRING, description='Отчество'),
        'phone': openapi.Schema(type=openapi.TYPE_STRING, description='Моб. телефон'),
    },
)
SCHEDULE_MODEL_FOR_GET_REQUESTS = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'tt_name': openapi.Schema(type=openapi.TYPE_STRING, description='Название ТТ'),
        'curator_email': openapi.Schema(type=openapi.TYPE_STRING, description='Почта куратора'),
        'curator_phone': openapi.Schema(type=openapi.TYPE_STRING, description='Телефон куратора'),
        'work_start_date': openapi.Schema(type=openapi.TYPE_STRING, description='Дата начала работы'),
        'visit_duration': openapi.Schema(type=openapi.TYPE_INTEGER, description='Продолжительность визита'),
        'vacation_start': openapi.Schema(type=openapi.TYPE_STRING, description='Дата начала отпуска/больничного'),
        'vacation_end': openapi.Schema(type=openapi.TYPE_STRING, description='Дата окончания отпуска/больничного'),
        'provider_name': openapi.Schema(type=openapi.TYPE_STRING, description='Наименование поставщика'),
        'visit_weekday': openapi.Schema(type=openapi.TYPE_STRING, description='День недели посещения'),
        'work_shifts': openapi.Schema(type=openapi.TYPE_INTEGER, description='Количество рабочих смен'),
        'weekend_shifts': openapi.Schema(type=openapi.TYPE_INTEGER, description='Количество выходных смен'),
        'is_active': openapi.Schema(type=openapi.TYPE_BOOLEAN),
        'is_vacation': openapi.Schema(type=openapi.TYPE_BOOLEAN),
        'is_sick': openapi.Schema(type=openapi.TYPE_BOOLEAN)
    },
)

# сущность фиксированного графика. основана на api.models.Schedule
FIXED_SCHEDULE = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'tt_name': openapi.Schema(type=openapi.TYPE_STRING, description='Название ТТ'),
        'provider_name': openapi.Schema(type=openapi.TYPE_STRING, description='Наименование поставщика'),
        'curator_email': openapi.Schema(type=openapi.TYPE_STRING, description='Почта куратора'),
        'curator_phone': openapi.Schema(type=openapi.TYPE_STRING, description='Телефон куратора'),
        'work_start_date': openapi.Schema(type=openapi.TYPE_STRING, description='Дата начала работы'),
        'visit_duration': openapi.Schema(type=openapi.TYPE_INTEGER, description='Продолжительность визита'),
        'visit_weekday': openapi.Schema(type=openapi.TYPE_STRING, description='День недели посещения'),
    },
    required=[
        'tt_name', 'provider_name', 'curator_email', 'curator_phone',
        'work_start_date', 'visit_duration', 'visit_weekday'
    ]
)

# сущность плавающего графика. основана на api.models.Schedule
SLIDING_SCHEDULE = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'tt_name': openapi.Schema(type=openapi.TYPE_STRING, description='Название ТТ'),
        'provider_name': openapi.Schema(type=openapi.TYPE_STRING, description='Наименование поставщика'),
        'curator_email': openapi.Schema(type=openapi.TYPE_STRING, description='Почта куратора'),
        'curator_phone': openapi.Schema(type=openapi.TYPE_STRING, description='Телефон куратора'),
        'work_start_date': openapi.Schema(type=openapi.TYPE_STRING, description='Дата начала работы'),
        'visit_duration': openapi.Schema(type=openapi.TYPE_INTEGER, description='Продолжительность визита'),
        'work_shifts': openapi.Schema(type=openapi.TYPE_INTEGER, description='Количество рабочих смен'),
        'weekend_shifts': openapi.Schema(type=openapi.TYPE_INTEGER, description='Количество выходных смен'),
    },
    required=[
        'tt_name', 'provider_name', 'curator_email', 'curator_phone',
        'work_start_date', 'visit_duration', 'work_shifts', 'weekend_shifts'
    ]
)

# сущность графика на увольнение. основана на api.models.Schedule
DISMISSAL_SCHEDULE = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'tt_name': openapi.Schema(type=openapi.TYPE_STRING, description='Название ТТ'),
        'provider_name': openapi.Schema(type=openapi.TYPE_STRING, description='Наименование поставщика'),
    },
    required=['tt_name', 'provider_name']
)

# сущность графика на отпуск/больничный. основана на api.models.Schedule
VACATION_SICK_SCHEDULE = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'tt_name': openapi.Schema(type=openapi.TYPE_STRING, description='Название ТТ'),
        'provider_name': openapi.Schema(type=openapi.TYPE_STRING, description='Наименование поставщика'),
        'vacation_start': openapi.Schema(type=openapi.TYPE_STRING, description='Дата начала отпуска/больничного'),
        'vacation_end': openapi.Schema(type=openapi.TYPE_STRING, description='Дата окончания отпуска/больничного')
    },
    required=['tt_name', 'provider_name', 'vacation_start', 'vacation_end']
)


# ОПИСАНИЕ МОДЕЛЕЙ ТЕЛА КАЖДОГО ЗАПРОСА НА ВСТАВКУ ДАННЫХ
# /schedules/fixed/open-access/, /schedules/fixed/change-schedule/
REQUEST_BODY_FIXED_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE_MODEL = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'merchandiser': MERCHANDISER_POST,
        'schedules': openapi.Schema(
            title='Графики мерчендайзера',
            description='Массив',
            type=openapi.TYPE_ARRAY,
            items=FIXED_SCHEDULE
        )
    }
)
REQUEST_BODY_FIXED_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE = openapi.Schema(
    title='Пример тела запроса',
    description='В запросе передается массив из составных JSON документов: '
                'одиночный JSON с данными мерчендайзера и массив JSON документов с его графиками',
    type=openapi.TYPE_ARRAY,
    items=REQUEST_BODY_FIXED_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE_MODEL
)

# /schedules/sliding/open-access/, /schedules/sliding/change-schedule/
REQUEST_BODY_SLIDING_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE_MODEL = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'merchandiser': MERCHANDISER_POST,
        'schedules': openapi.Schema(
            title='Графики мерчендайзера',
            description='Массив',
            type=openapi.TYPE_ARRAY,
            items=SLIDING_SCHEDULE
        )
    }
)
REQUEST_BODY_SLIDING_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE = openapi.Schema(
    title='Пример тела запроса',
    description='В запросе передается массив из составных JSON документов: '
                'одиночный JSON с данными мерчендайзера и массив JSON документов с его графиками',
    type=openapi.TYPE_ARRAY,
    items=REQUEST_BODY_SLIDING_SCHEDULE_OPEN_ACCESS_CHANGE_SCHEDULE_MODEL
)

# /schedules/dismissal/
REQUEST_BODY_DISMISSAL_SCHEDULE_MODEL = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'merchandiser': MERCHANDISER_POST,
        'schedules': openapi.Schema(
            title='Графики мерчендайзера',
            description='Массив',
            type=openapi.TYPE_ARRAY,
            items=DISMISSAL_SCHEDULE
        )
    }
)
REQUEST_BODY_DISMISSAL_SCHEDULE = openapi.Schema(
    title='Пример тела запроса',
    description='В запросе передается массив из составных JSON документов: '
                'одиночный JSON с данными мерчендайзера и массив JSON документов с его графиками',
    type=openapi.TYPE_ARRAY,
    items=REQUEST_BODY_DISMISSAL_SCHEDULE_MODEL
)


# /schedules/vacation-sick/
REQUEST_BODY_VACATION_SICK_MODEL = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'merchandiser': MERCHANDISER_POST,
        'schedules': openapi.Schema(
            title='Графики мерчендайзера',
            description='Массив',
            type=openapi.TYPE_ARRAY,
            items=VACATION_SICK_SCHEDULE
        )
    }
)
REQUEST_BODY_VACATION_SICK = openapi.Schema(
    title='Пример тела запроса',
    description='В запросе передается массив из составных JSON документов: '
                'одиночный JSON с данными мерчендайзера и массив JSON документов с его графиками',
    type=openapi.TYPE_ARRAY,
    items=REQUEST_BODY_VACATION_SICK_MODEL
)

# ОПИСАНИЕ ОТВЕТОВ НА ЗАПРОСЫ

# /schedules/processing-result/
BARCODE = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'merchandiser': openapi.Schema(type=openapi.TYPE_STRING, description='ФИО мерчендайзера'),
        'full_info': openapi.Schema(type=openapi.TYPE_STRING, description='URL для получения полной информации о мерчендайзере')
    }
)
DETAILS_GET_RESPONSE_PROCESSING_RESULT = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'status': openapi.Schema(type=openapi.TYPE_STRING, description='Статус выполнения запроса'),
        'errors': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_STRING), description='Ошибки'),
        'barcodes': openapi.Schema(type=openapi.TYPE_ARRAY, items=BARCODE)
    }
)
GET_RESPONSE_PROCESSING_RESULT = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'request_guid': openapi.Schema(type=openapi.TYPE_STRING, description='GUID запроса'),
        'timestamp': openapi.Schema(type=openapi.TYPE_STRING, description='Timestamp выполнения запроса'),
        'details': DETAILS_GET_RESPONSE_PROCESSING_RESULT
    }
)

# /merchandisers/all/
GET_MERCHANDISERS_ALL = openapi.Schema(
    title='Ответ',
    type=openapi.TYPE_OBJECT,
    properties={
        'request_guid': openapi.Schema(type=openapi.TYPE_STRING, description='GUID запроса'),
        'timestamp': openapi.Schema(type=openapi.TYPE_STRING, description='Время выполнения запроса'),
        'details': openapi.Schema(type=openapi.TYPE_ARRAY, items=MERCHANDISER_GET)
    }
)

# /merchandisers-schedules/
GET_MERCHANDISERS_SCHEDULES_MODEL = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'merchandiser': MERCHANDISER_GET,
        'schedules': openapi.Schema(type=openapi.TYPE_ARRAY, items=SCHEDULE_MODEL_FOR_GET_REQUESTS)
    }
)

GET_MERCHANDISERS_SCHEDULES = openapi.Schema(
    title='Ответ',
    type=openapi.TYPE_OBJECT,
    properties={
        'request_guid': openapi.Schema(type=openapi.TYPE_STRING, description='GUID запроса'),
        'timestamp': openapi.Schema(type=openapi.TYPE_STRING, description='Timestamp выполнения запроса'),
        'details': openapi.Schema(type=openapi.TYPE_ARRAY, items=GET_MERCHANDISERS_SCHEDULES_MODEL)
    }
)

# /merchandisers-barcodes-schedules/
GET_MERCHANDISERS_SCHEDULES_BARCODES_MODEL = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'merchandiser': MERCHANDISER_GET,
        'barcode_binary': openapi.Schema(type=openapi.TYPE_STRING),
        'schedules': openapi.Schema(type=openapi.TYPE_ARRAY, items=SCHEDULE_MODEL_FOR_GET_REQUESTS),
    }
)
GET_MERCHANDISERS_SCHEDULES_BARCODES = openapi.Schema(
    title='Ответ',
    type=openapi.TYPE_OBJECT,
    properties={
        'request_guid': openapi.Schema(type=openapi.TYPE_STRING, description='GUID запроса'),
        'timestamp': openapi.Schema(type=openapi.TYPE_STRING, description='Timestamp выполнения запроса'),
        'details': openapi.Schema(type=openapi.TYPE_ARRAY, items=GET_MERCHANDISERS_SCHEDULES_BARCODES_MODEL)
    }
)

# /get-one-merchandiser-info/
GET_ONE_MERCHANDISER_INFO = openapi.Schema(
    title='Ответ',
    type=openapi.TYPE_OBJECT,
    properties={
        'request_guid': openapi.Schema(type=openapi.TYPE_STRING, description='GUID запроса'),
        'timestamp': openapi.Schema(type=openapi.TYPE_STRING, description='Timestamp выполнения запроса'),
        'details': openapi.Schema(type=openapi.TYPE_ARRAY, items=GET_MERCHANDISERS_SCHEDULES_BARCODES_MODEL)
    }
)

POST_RESPONSE_MODEL = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'request_guid': openapi.Schema(type=openapi.TYPE_STRING, description='GUID запроса'),
        'timestamp': openapi.Schema(type=openapi.TYPE_STRING, description='Timestamp выполнения запроса'),
        'status': openapi.Schema(type=openapi.TYPE_STRING, description='Статус'),
        'errors': openapi.Schema(type=openapi.TYPE_STRING, description='Ошибки')
    }
)
POST_RESPONSE = openapi.Response(description='Пример ответа сервера', schema=POST_RESPONSE_MODEL)
